
public class ARRAY_DEMO_4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//

		int[][] anArray;
		anArray = new int[4][7];

		int counter = 1;

		for (int row = 0; row < anArray.length; row++) {
			for (int col = 0; col < anArray[0].length; col++) {
				anArray[row][col] = counter;
				counter++;
			}
		}
		printMatrix(anArray);
	}

	public static void printMatrix(int[][] m) {
		for (int row = 0; row < m.length; row++) {
			System.out.print(m[row][0]);
			for (int col = 1; col < m[0].length; col++) {
				System.out.print(", " + m[row][col]);
			}
			System.out.println();
		}
	}

}
