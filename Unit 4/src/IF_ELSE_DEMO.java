import java.util.Scanner;
//
public class IF_ELSE_DEMO {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int testScore;
		char grade;
		
		System.out.println("Enter a grade");
		Scanner inputValue;
		inputValue=new Scanner(System.in);
		testScore = inputValue.nextInt();
		inputValue.close();
		if (testScore >= 90) {
			grade = 'A';
		} else {
			if (testScore >= 80) {
				grade = 'B';
			} else {
				if (testScore >= 70) {
					grade = 'C'; 
				} else {
					if (testScore >= 60) { 
						grade = 'D';
					} else {
						grade = 'F';
					}
				}
			}
		}      System.out.println("Grade = " + grade);
	}

}
