//
public class Tandem1 extends Bicycles {
	private int numSeats = 2;
	
	void applyBrakes(int decrement1, int decrement2) {
		applyBrakes(decrement1);
		applyBrakes(decrement2);
	}
	
	void printStates() {
		super.printStates();
		System.out.println("numSeats: "+numSeats);
	}
}
