//
public class Main2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bicycles bike1 = new Bicycles();
		Bicycles bike2 = new Bicycles();

		bike1.changeCadence(50);
		bike1.changeSpeedUp(10);
		bike1.changeGear(2);
		bike1.printStates();
		
		bike2.changeCadence(50);
		bike2.changeSpeedUp(10);
		bike2.changeGear(2);
		bike2.changeCadence(40);
		bike2.changeSpeedUp(10);
		bike2.changeGear(3);
		bike2.applyBrakes(5);
		bike2.printStates();
		
		bike1.printStates();
		bike2.printStates();
	}

}
