import java.util.Scanner;
//
public class EXERCICE_ARRAY_7_CLASES {

	public static final int NUM_SHIPS = 20;
	public static final int DIMENSION = 10;
	public static final int x = (DIMENSION*2)+1;
	public static int cont;
	public static int lastrow = 0;
	public static char c = 'A';

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		char[][] array = new char[DIMENSION][DIMENSION];

		initArray(array);
		printMatrix(array);
		addShips(array);

		cont = 0;
		int num;
		char letter;
		int row;
		int col;
		while (cont <= NUM_SHIPS) {
			System.out.print("Enter the row(letter): ");
			letter = input.next().charAt(0);
			row = (letter + 1) - 'A';
			System.out.print("Enter the col(number): ");
			num = input.nextInt();
			col = num;
			System.out.print("\n");
			if (array[row][col] == '1') {
				array[row][col] = 'X';
				cont++;
			} else {
				array[row][col] = 'O';
			}
			lastrow = 0;
			printMatrix(array);
		}

		input.close();
	}

	private static void initArray(char[][] m) {
		for (int row = 0; row < m.length; row++) {
			for (int col = 0; col < m[0].length; col++) {
				m[row][col] = ' ';
			}

		}
	}

	public static void printMatrix(char[][] m) {
		for (int i = 1; i <= x; i++) {
			if (i == 1) {
				printCabecera(m);
			} else {
				if (i == x) {
					printPie(m);
				} else {
					if (i % 2 == 0) {
						printArray(m);
					} else {
						printSeparacion(m);
					}
				}
			}

		}
	}

	private static void addShips(char[][] m) {
		cont = 0;
		while (cont <= NUM_SHIPS) {
			int x = (int) (Math.random() * (DIMENSION - 1) + 1);
			int y = (int) (Math.random() * (DIMENSION - 1) + 1);
			if (m[y][x] == ' ') {
				m[x][y] = '1';
				cont++;
			}
		}
	}

	private static void printCabecera(char[][] m) {
		cont = 1;
		while (cont <= x) {
			if (cont == 1) {
				System.out.print("\u2554");// Esquina superior izquierda
				cont++;
			} else {
				if (cont == x) {
					System.out.println("\u2557");// Esquina superior derecha
					cont++;
				} else {
					if (cont % 2 == 0) {
						System.out.print("\u2550" + "\u2550" + "\u2550");// Horintal basica
						cont++;
					} else {
						System.out.print("\u2566");// Horizontal hacia abajo
						cont++;
					}
				}
			}

		}
	}

	private static void printPie(char[][] m) {
		cont = 1;
		while (cont <= x) {
			if (cont == 1) {
				System.out.print("\u255A");// Esquina superior izquierda
				cont++;
			} else {
				if (cont == x) {
					System.out.println("\u255D");// Esquina superior derecha
					cont++;
				} else {
					if (cont % 2 == 0) {
						System.out.print("\u2550" + "\u2550" + "\u2550");// Horintal basica
						cont++;
					} else {
						System.out.print("\u2569");// Horizontal hacia abajo
						cont++;
					}
				}
			}

		}
	}

	private static void printArray(char[][] m) {
		for (int row = lastrow; row < m.length;) {
			if (row == 0) {
				System.out.print("\u2551" + "   ");
			} else {
				System.out.print("\u2551 " + c + " ");
				c++;
			}
			for (int col = 1; col < m[0].length; col++) {
				if (row == 0) {
					System.out.print("\u2551 " + col + " ");
				} else {
					if (m[row][col] == '1') {
						System.out.print("\u2551" + "   ");
					} else {
						System.out.print("\u2551 " + m[row][col] + " ");
					}
				}
			}
			System.out.println("\u2551");
			if (row == (m.length - 1)) {
				c = 'A';
			}
			lastrow++;
			break;
		}
	}

	private static void printSeparacion(char[][] m) {
		cont = 1;
		while (cont <= x) {
			if (cont == 1) {
				System.out.print("\u2560");// Vertical derecha
				cont++;
			} else {
				if (cont == x) {
					System.out.println("\u2563");// Vertical izquierda
					cont++;
				} else {
					if (cont % 2 == 0) {
						System.out.print("\u2550" + "\u2550" + "\u2550");// Horintal basica
						cont++;
					} else {
						System.out.print("\u256C");// Cruz
						cont++;
					}
				}
			}

		}
	}
}
