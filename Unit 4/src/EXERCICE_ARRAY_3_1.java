//
public class EXERCICE_ARRAY_3_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[][] array;
		array = new int[10][10];
		for (int row = 0; row < array.length; row++) {
			for (int col = 0; col < array[0].length; col++) {
				array[row][col] = 0;
			}
		}
		array[0][4] = 1;
		array[2][6] = 1;
		array[3][1] = 1;
		array[8][6] = 1;
		printMatrix(array);
	}
	
	public static void printMatrix(int[][] m) {
		for (int row = 0; row < m.length; row++) {
			System.out.print(m[row][0]);
			for (int col = 1; col < m[0].length; col++) {
				System.out.print(" " + m[row][col]);
			}
			System.out.println();
		}
	}
}
