//
public class EXERCICE_ARRAY_6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] array = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
		int comp;
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length; j++) {
				if (array[i] < array[j]) {
					comp = array[i];
					array[i] = array[j];
					array[j] = comp;
				}
			}
		}
		for (int k = 0; k < array.length; k++) {
			if (k != (array.length - 1)) {
				System.out.print(array[k] + ", ");
			} else {
				System.out.println(array[k]);
			}
		}
	}

}