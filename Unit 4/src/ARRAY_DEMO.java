
public class ARRAY_DEMO {

	public static final int NUM_ELEMENTS = 10;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//

		// declares an array of integers
		int[] anArray;
		// allocates memory for 10 integers
		anArray = new int[NUM_ELEMENTS];
		for (int i = 0; i < anArray.length; i++)
			anArray[i] = (i + 1) * 100;
		for (int i = 0; i < anArray.length; i++)
			System.out.println("Element at index " + i + ": " + anArray[i]);
	}
}