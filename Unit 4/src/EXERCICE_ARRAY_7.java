import java.util.Scanner;
//
public class EXERCICE_ARRAY_7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		char[][] array = { { ' ', '1', '2', '3', '4', '5', '6', '7', '8' },
				{ 'A', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', }, { 'B', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', },
				{ 'C', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', }, { 'D', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', },
				{ 'E', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', }, { 'F', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', },
				{ 'G', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', }, { 'H', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', } };
		char[][] array2 = { { ' ', '1', '2', '3', '4', '5', '6', '7', '8' },
				{ 'A', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', }, { 'B', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', },
				{ 'C', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', }, { 'D', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', },
				{ 'E', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', }, { 'F', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', },
				{ 'G', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', }, { 'H', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', } };

		printMatrix(array);
		
		int x = (int) (Math.random() * 8 + 1);
		int y = (int) (Math.random() * 8 + 1);
		int cont = 0;
		while (cont <= 10) {
			x = (int) (Math.random() * 8 + 1);
			y = (int) (Math.random() * 8 + 1);
			if (array[y][x] == ' ') {
				array[x][y] = '1';
				cont++;
			}
		}

		cont = 0;
		int num;
		char letter;
		int row;
		int col;
		while (cont <= 10) {
			System.out.print("Enter the row(letter): ");
			letter = input.next().charAt(0);
			row = (letter + 1) - 'A';
			System.out.print("\n"); // System.out.println(row);
			System.out.print("Enter the col(number): ");
			num = input.nextInt();
			col = num;
			System.out.print("\n");
			if (array[row][col] == '1') {
				array2[row][col] = 'X';
				cont++;
			} else {
				array2[row][col] = 'O';
			}

			printMatrix(array2);
		}

		input.close();
	}

	public static void printMatrix(char[][] m) {
		int x = (((m[0].length) * 2) + 1);
		int lastrow = 0;
		for (int i = 1; i <= x; i++) {
			int cont = 1;
			if (i == 1) {
				while (cont <= x) {
					if (cont == 1) {
						System.out.print("\u2554");// Esquina superior izquierda
						cont++;
					} else {
						if (cont == x) {
							System.out.println("\u2557");// Esquina superior derecha
							cont++;
						} else {
							if (cont % 2 == 0) {
								System.out.print("\u2550");// Horintal basica
								cont++;
							} else {
								System.out.print("\u2566");// Horizontal hacia abajo
								cont++;
							}
						}
					}

				}
			} else {
				if (i == x) {
					while (cont <= x) {
						if (cont == 1) {
							System.out.print("\u255A");// Esquina superior izquierda
							cont++;
						} else {
							if (cont == x) {
								System.out.println("\u255D");// Esquina superior derecha
								cont++;
							} else {
								if (cont % 2 == 0) {
									System.out.print("\u2550");// Horintal basica
									cont++;
								} else {
									System.out.print("\u2569");// Horizontal hacia abajo
									cont++;
								}
							}
						}

					}
				} else {
					if (i % 2 == 0) {
						for (int row = lastrow; row < m.length;) {
							System.out.print("\u2551" + m[row][0]);
							for (int col = 1; col < m[0].length; col++) {
								System.out.print("\u2551" + m[row][col]);
							}
							System.out.println("\u2551");
							lastrow++;
							break;
						}
					} else {
						while (cont <= x) {
							if (cont == 1) {
								System.out.print("\u2560");// Vertical derecha
								cont++;
							} else {
								if (cont == x) {
									System.out.println("\u2563");// Vertical izquierda
									cont++;
								} else {
									if (cont % 2 == 0) {
										System.out.print("\u2550");// Horintal basica
										cont++;
									} else {
										System.out.print("\u256C");// cruz
										cont++;
									}
								}
							}

						}
					}
				}
			}
		}
	}
}
